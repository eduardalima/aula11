public class ProfHorista extends Professor
{
	private int total_horas;
	private String salario_hora;
	
	super(String nome, String matricula, int idade);
	
	public ProfHorista(int total_horas, String salario_hora)
	{
		this.total_horas = total_horas;
		this.salario_hora = salario_hora;
	}
	
	public int getTotalHoras()
	{
		return total_horas;
	}
		
	public void setTotalHoras(int total_horas)
	{
		this.total_horas = total_horas;
	} 
	
	public String getSalarioHora()
	{
		return salario_hora;
	}
		
	public void setSalarioHora(String salario_hora)
	{
		this.salario_hora = salario_hora;
	} 
	
	public void calcSalario()
	{
		private int salHora_int, salario;
		
		salHora_int = parseInt(salario_hora);
		salario = salHora_int * total_horas;
		
		return salario;
	}
}
