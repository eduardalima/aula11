import java.util.ArrayList;

public class Database
{
	private ArrayList<Cliente> clientes = new ArrayList<Cliente>();
    
    public void addCliente(Cliente cliente)
    {
  		clientes.add(cliente);
    }
    
    public Cliente getCliente(int i)
    {
    	return ArrayList[i];
    }
    
    public int numClientes()
    {
  		return clientes.size();
    }
    
    public int numPessoaFisica()
    {
    	int count;
        
    	for(Cliente cliente : clientes)
        {
  			if(cliente instanceOf PessoaFisica)
        	{
        		count++;
        	}
        }
        
        return count;
    }
    
    public int numPessoaJuridica()
    {
  		int count;
        
    	for(Cliente cliente : clientes)
        {
  			if(cliente instanceOf PessoaJuridica)
        	{
        		count++;
        	}
        }
        
        return count;
    }
}
